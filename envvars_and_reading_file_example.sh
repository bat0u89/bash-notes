#!/bin/bash
ENVIRONMENT=$1
PARTNER=$2
PUBLIC_KEY="$(cat ./test_pub_key_json_friendly)"

printf '%s' "$PUBLIC_KEY"

command --parameter-overrides SftpUsername="$PARTNER" SftpLambdaStackName="$ENVIRONMENT-lambda-stack" 